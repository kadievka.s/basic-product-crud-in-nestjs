import { Body, Controller, Delete, Get, Param, Patch, Post } from '@nestjs/common';
import { ProductsService } from './products.service';

@Controller('products')
export class ProductsController {
    constructor(private readonly productsService: ProductsService){}

    @Post()
    async addProduct(@Body() body: {title: string, description: string, price: number}) {
        const generatedId = await this.productsService.insertProduct(body.title, body.description, body.price);
        return {id: generatedId};
    }

    @Get()
    async getAllProducts(){
        const products = await this.productsService.getProducts();
        return products.map(p => ({
            id: p.id,
            title: p.title,
            description: p.description,
            price: p.price
        }));
    }

    @Get(':id')
    async getProduct(@Param('id') prodId: string){
        return await this.productsService.getSingleProduct(prodId);
    }

    @Patch(':id')
    async updateProduct(@Param('id') prodId: string, @Body() body: {title: string, description: string, price: number}){
        return await this.productsService.updateProduct(prodId, body.title, body.description, body.price);
    }

    @Delete(':id')
    async deleteProduct(@Param('id') prodId: string){
        return await this.productsService.deleteProduct(prodId);
    }
}