import { Injectable, NotFoundException } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";

import { Product } from "./product.model";

@Injectable()
export class ProductsService {
    private products: Product[] = [];

    constructor(@InjectModel('Product') private readonly productModel: Model<Product>) { }

    async insertProduct(title: string, description: string, price: number) {
        try {
            const newProduct = new this.productModel({ title, description, price });
            const result = await newProduct.save();
            return result.id as string;
        } catch (error) {
            throw error;
        }
    }

    async getProducts() {
        const products = await this.productModel.find();
        return products as Product[];
    }

   async  getSingleProduct(id: string) {
       const product = await this.findProduct(id);
        return {
            id: product.id,
            title: product.title,
            description: product.description,
            price: product.price
        };
    }

    async updateProduct(id: string, title: string, desc: string, price: number) {
        const updatedProduct = await this.findProduct(id);
        if(title) updatedProduct.title = title;
        if(desc) updatedProduct.description = desc;
        if(price) updatedProduct.price = price;
        return await updatedProduct.save();
    }

    async deleteProduct(id: string) {
        let product: Product;
        try {
            product = await this.productModel.findByIdAndDelete(id);
            return {
                id: product.id,
                title: product.title,
                description: product.description,
                price: product.price
            };
        } catch (error) {
            throw new NotFoundException('Could not find the product.');
        }
    }

    private async findProduct(id: string): Promise<Product> {
        let product: Product;
        try {
            product = await this.productModel.findById(id);
        } catch (error) {
            throw new NotFoundException('Could not find the product.');
        }
        if (!product) {
            throw new NotFoundException('Could not find the product.');
        }
        return product;
    }
}